# Intercessor Example

An example app for [Intercessor][].

## Usage

Clone this repo. Build and run it by typing:

    npm run up

Go to [localhost:3000](http://localhost:3000) to see it. See other [supported
commands][sc].

## License

MIT

[Intercessor]: https://github.com/paul-nechifor/intercessor
[sc]: https://github.com/paul-nechifor/intercessor#supported-commands
